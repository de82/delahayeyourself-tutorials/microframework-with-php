# 📣 Disclaimer

> Le repository git que vous êtes en train de visualiser n'a eu qu'une vocation pédagogique pour accompagner les modules enseignés en DUT 1A, 2A, LPRGI et 1A de cycle ingénieur.

> Son propos n'avait qu'une portée pédagogique et peut ne pas refléter l'état actuel ou encore les bonnes pratiques de la/les technologie(s) utilisée. 

## 🎯 Utilisation

Vous êtes libre de réutiliser librement le code présent dans ce repository. Prenez garde que le code est ici daté, non mis à jour et potentiellement ouvert aux failles et/ou bugs.

## 🦕 Crédit

[Samy Delahaye](https://delahayeyourself.info)


## 🪴 Descriptif du projet


### Microframework with php

Un projet pour apprendre le MVC avec un microframework et quelques lib bien utile

## How ?

$ git clone this project
$ composer install
$ php -S 127.0.0.1:5000
