<?php

require "vendor/autoload.php";

$response = Requests::get('https://allosaurus.delahayeyourself.info/api/books/');

if($response->status_code == 200):
    
    $books = json_decode($response->body);

    foreach($books as $book): ?>
        <h2><?php echo($book->title); ?></h2>
        <img src="<?php echo($book->cover); ?>" />
    <?php endforeach;
    
endif;